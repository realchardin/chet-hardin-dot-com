The Difference a Day Made

By Chet Hardin

Photos by Chris Shields

Four Capital Region residents, their lives forever altered by 9/11, tell their stories

 

In the warm summer�s end, the door is propped open to Pi Naturals. It�s busy. Ty Austin, co-owner, a floral artist, has just sold a silver, plasticine horse, coated in glitter and adorned with shards of mirror. He jokes with the woman who bought the shimmering equine, saying that he wants to meet the lucky donee. �They gotta be cool.�

Pi Naturals is housed in what was once a run-down little one-story building on the south side of Hoosick Street in Troy, in a charming but distressed neighborhood. Austin�s business partner, Jerry Ellis, bought the building for next to nothing, and the two of them went to work. Gutting the entire brick structure, they tore out all of the street-level joists, ripped out the floor, tore off the rain-damaged roof, and stripped the thick plaster from the brick walls. When they were finished with the demolition, what was left was an open space, two stories high from the basement�s dirt floor to the missing roof.

Into this auspicious space they poured a cement floor, installed a large walk-in cooler for flowers, a work sink, and a set of stairs. They rebuilt the first floor as an open loft that leads from the front door to a small sitting area in the rear of the building. The store�s configuration offers an open, yet intimate setting, and on this particularly busy day, the shop has adopted the quality of its community�neighbors stop and talk, friends catch up with one another.

Austin is working fast and determined. He is headed to a weekend of motorcycle races in New Hampshire.

�I�m really excited. I grew up next to a drag strip, where they have the Winston World Nationals, so I really like that stuff,� Austin says, adding, �I haven�t gone to something like that in years.�

Austin grew up in Norwalk, Ohio, a town of 30,000 people, along the coast of Lake Erie.

�I grew up like a total hick,� he says. �Country boy.�

This country boy attended the University of Toledo, where he played men�s volleyball and studied biomedical engineering. It seems like a noble profession, he says, developing prosthetics limbs that can give people back their ability to walk.

But this field, with its focus on high mathematics, wasn�t where he was headed. With the help of a small revelation, Austin concluded that his destiny lie in something a little less esoteric.

�I was sitting there in calculus five. Do you know what they do in calc five? They take the X, Y, and Z plain, which is the 3-D plain,� he explains. �Then they find the molecule, and they revolve it around one of the axes, and then they take a slice of that. . . . There is the area of the cone.� It has been a while since that class, and his eyes widen. Trying to remember the abstruse science overwhelms him.

�And then they grade you on it, and you don�t even know if that�s the right answer because it�s a theorem.�

�Here�s an argument for Joshua!� Austin interrupts himself by shouting to the couple sitting outside on a bench. �I have an argument for him,� Austin teases, as he strips the leafs from the stem of a flower. �He says math is the universal language. I�m like, �Are you fucking nuts?� �

Flowers may not be a universal language either, Austin admits, but he is convinced that they offer something akin to universality. When anyone sees a flower, he says with a smile, they feel a moment of utter happiness.

�It could be only for a second,� he says. �But that�s what we need. We have to remember those moments.�

After college, Austin met a guy from New York City and decided to move out east with him. He moved to Manhattan and settled into an apartment on Canal Street, where he was when two hijacked airplanes destroyed the Twin Towers on Sept. 11, 2001. The apartment was just a few blocks away from Ground Zero. �It stunk. It stunk to high heaven. For months . . . experiencing it, smelling it, and probably being possessed by the whole thing. There was a lot of energy down there. The energy�you could touch it. You could feel it.�

Austin had been sitting on his balcony the morning of Sept. 11. He remembers watching people plummet from the upper floors of the World Trade Center towers, having made, he believes, an unimaginable decision. �You have to wonder what was going on in their heads . . . At that moment, you are like, �Oh my god, do I stay here and burn? Or do I jump and take my chances?� And you know there are no chances.�

Austin watched as tens of thousands of people walked north away from Ground Zero. �At the beginning, they were clean-cut but panicked,� he says. �Further on, they were covered in soot and screaming their heads off. And the ones after that were all bloody.�

He moved to the Capital Region eight months later.

 
Jerry Ellis
Jerry Ellis, co-owner of Pi Naturals, was working as a bar back at Club Phoenix in Albany when he met his future business partner, Austin. �Ty jumped over the bar one day,� Ellis says, �put his arms around my neck and wouldn�t let go.�

Ellis had moved to the Capital Region in September 2002, and was working as a substitute teacher. The bar helped supplement his income. Someone suggested that he get a real-estate license, and he did so in the hopes of finding the perfect fixer-upper. Which he did, he says, when he found the Pi Naturals building. At least, the price was right.

And the time, he says, has flown.

�Some days have gone on and on forever. But, looking back, I have to do the math to believe it�s been four years. And five years since September 11 . . .�

Five years ago, Ellis was living on East 65th Street, just three blocks from Central Park. A perfect distance to ride his bike to the park, he says. He worked for a large international law firm, Davis, Polk and Wardwell, first as a recruiter, then in business development. He lived in the same apartment and worked at the same job for about nine years.

�My friends downtown,� he says, �all thought that was the suburbs.�

He was also working toward his master�s degree in elementary education, and in his spare time would student teach. On Sept. 11, he was teaching third grade at a public school on the Upper East Side. At first, no one really knew what was going on.

�A lot of parents started coming to pick up their kids before we really understood what was going on,� he says. �About midway through the morning, we were starting to hear some more specific rumors. Finally, I got through to my father. I was standing in the boy�s room of the school, and I heard it from him what had happened.�

�So I walked outside, and walked down to Fifth Avenue, and looked straight down,� he recalls. �You could see this unbelievable cloud hanging over the lower tip of Manhattan. It was five or six miles away . . . where the horizon starts to bend . . . �

He had to stay at the school until every child had been picked up by a parent. That took all morning. By 1 PM, there were still eight kids left.

When he finally could leave the school, he started walking home.

�I had to walk 20 blocks south,� he says, �and everybody, the masses were streaming north, because the transit was shut down. So everybody was walking the opposite direction I was. It was horrible.�

It was then that he decided he would leave New York City. A year later, he moved.

�It changed the course of my life,� Ellis says of 9/11. �I was in a graduate program for teaching elementary school, and I couldn�t get focused on it again. I walked around the streets and looked up at the sky. I just couldn�t focus.�

 
Carmen Gonzalez
�It was the feeling of being powerless,� says Carmen Gonzalez. �Power- less to move, powerless to leave, powerless to help, powerless to talk to my children.�

She had been at work that morning. Her office was on the West Side, near Penn Station. �I heard the rumble before I saw the towers collapse,� she says. �I close my eyes now and it comes back. . . . Every second is frozen in time. . . . And I remember as each layer fell, I felt my cells close down. I just went totally numb and ice-cold, and I couldn�t stop shaking.�

Gonzalez was living in Long Beach, Long Island, at the time, five blocks from the Long Island Railroad and five blocks from the ocean. She would commute to work by taking the train into Penn Station. It�s about a 50-minute train ride to the city. From Penn Station, she could walk to work.

It was a very comfortable life, she remembers.

But on Sept. 11, no one knew when the trains would start running again. To get home, all she could do was get on a train and wait. �I couldn�t stop crying,� she says. �It kept coming out.�

There were guards in black uniforms with machine guns and dogs. �And when the doors closed, I thought, �Oh my God. Oh my God, I am going into a tunnel.� That�s when it hit me, I was going into a tunnel. I had a panic attack, which I never have.�

The four minutes and 32 seconds she spent in that tunnel was the worst time of her life. She kept thinking: �I have no way out. I have nowhere to go. The train is moving�I have no way even to get off the train.� That feeling of panic lasted for almost a year, she says. Whenever her commute took her through the tunnel, she would break out into a cold sweat.

The day after the attacks, Gonzalez went to the beach, a favorite spot to relax and meditate, to calm down. She thought she would go into �a peaceful place.� But when she got there, there were thousands of people on the boardwalk. The media were there. �It was a circus,� she says. On the horizon, in the water, she could see dozens of ships with their guns pointed up into the sky, ships that she had never seen before. Above her, fighter jets circled. This went on for months.

Living on Long Island, she was left with a constant feeling of being trapped, she says. If something were to happen, how could 20 million people evacuate an island? Over a few bridges, in a tunnel?

�We were sitting ducks,� Gonzalez says. �And I felt that way until I moved up here. It stayed, that feeling.�

�After 9/11,� she continues, �there was such a sadness in the city, such a sadness, such a black cloud. Everybody walked around like zombies for weeks and weeks and weeks. After we got back on our feet, it was still there. No matter how much it seems to get back to normal, it�s always there, in the back of our minds, on the back burner.�

It was not the way Gonzalez was used to living. She was born in Manhattan and raised in Hell�s Kitchen, a neighborhood that she says was mostly Italians at the time. Her mother, a Puerto Rican immigrant, ran a successful children�s clothing store in Manhattan, and this afforded Gonzalez a comfortable, middle-class childhood. Manhattan, she says, gave her the world, everything that she could possibly want�it was a city filled with opportunity. It was her city.

�I am so appreciative of growing up in Manhattan,� she says. �It was so much a part of me. It was great, and I miss it. I miss the freedom of running around and feeling safe. Or never even thinking about being safe.�

�I don�t know if any of us can ever really feel safe again,� she adds. �In my heart, I don�t believe that they [the terrorists] are done with us.�

Gonzalez moved to Troy in 2004, after visiting a friend in Clifton Park. Her friend�s husband worked at Rensselaer Polytechnic Institute, and he offered to take Gonzalez on a tour of the campus.

�And I came for a ride, and we drove around,� she says. �I pretty much fell in love with Troy.�

So she bought a little coffee shop on 1st Street, just south of Washington Park, and moved into the upstairs apartment. She lives there now with her mom, Juanita, whom everyone, she says, calls �Mamacita.�

They have four cats, she jokes, �that allow me to live with them.�

Carmen�s Caf� features the artwork of local artists in a lovingly appointed little dining room. Gonzalez says she wants her caf� to be a place where the community comes to share their lives and, to this end, she is happy to open her doors to after-hours film showings, tapas nights, poetry readings, book readings, whatever her neighbors want.

Sitting in her coffee shop, with Mamacita off in her daydreams, this refugee of Manhattan�s most traumatic moment opens her hands to grace and says, �Now, I have this.�

 
Anthony Aversano
It was during the trial of Zacarias Moussouai that Anthony Aversano, a Troy native, found an opportunity to express his conviction: End the cycle of violence.

Moussouai eventually would be found guilty of conspiring with the 9/11 hijackers, and in turn, with conspiring in the act that killed Aversano�s father, Louis Aversano Jr., who worked for Aon Corp. in the World Trade Center.

Our court system assumes that justice is served, Aversano says, when the most extreme punishment possible is leveled against an offender. In Moussouai�s case, that punishment would have been capital. Aversano joined other people who lost loved ones in testifying for the defense.

�It is easier to not say or do anything,� Aversano says. �But it takes great courage to find the compassion, the compassion in your heart, to see past the hatred and the anger, which is a process I have been through since losing my dad.�

It was important to Aversano, during the trial, to make it clear that he wasn�t testifying on the behalf of Moussouai, but to �speak a different voice,� he says. He testified to �look at a bigger picture, and where we are as Americans, and where we are in, I think, the history of our planet.�

�That if we continue to act, when violence strikes our lives personally, or strikes our communities, or strikes our country,� he says, �if we continue to want to respond to violence with violence, then that is not serving justice, it�s not serving peace. It�s only continuing the cycle of violence in our world. And so I spoke my voice to say, �No more. I do not condone continuing the cycles of violence, to continue the cycles of hatred.� �

In 1991, Aversano, then a recent college graduate, decided to go west. He bought a van and remembers saying to himself, �I�m going to go find my life out there.�

He gained employment in Las Vegas in the television-and-film industry and continued his education, working toward a master�s degree in broadcasting at the University of Nevada Las Vegas. After six years, he realized, �Wait a minute. I was on my way to California here.�

So he packed up, he says, and moved to the San Francisco Bay area.

It was while he was in California that he began to lose interest in his former professional pursuits and began to explore �a more spiritual path.� It was this path that would eventually bring him face-to-face with a troubling incompleteness in his life: his rocky relationship with his dad.

For six years, he says, he was using his father �as a great big target for why life wasn�t working, for blaming him for lots of areas of life. . . . I got to be right, I was making him wrong.�

�But the part I wasn�t seeing in doing that with him was what it was costing me in my life,� he says. �It was costing me dearly.�

It was in a workshop in 1999 that Aversano had �this awakening and transformation.� He called his father. �I simply called him up,� he says. �I apologized to him. I said, �Dad, I�m so sorry. I�ve been a jerk.� �

From that one phone call, he says, he forged a new relationship with his father.

�I had a connection with him that was beautiful, it was loving,� Aversano says. �We connected very deeply, and I feel really blessed, like God gave me the opportunity to have a lifetime�s worth of connecting with dad in those couple of years.�

The day he reconciled with his father was Sept. 11, 1999. Two years later, his father would be murdered. Anthony Aversano, who still lived in California at the time, has since returned to Troy.

�I�m really sad today,� Aversano says. �There are people dying. . . . When are we going to get it in America? I don�t know what to feel. I feel sad. I feel angry. I feel, I feel . . . that we are so disconnected as Americans. . . . We are so not present to what�s happening out there in the world. That our government is creating the same tragedies that I felt�there are people dying in Iraq every day that are people like you and me. There are little kids dying,� he says, his voice breaking.

�There are other young men losing their fathers.�

He stops and pauses. his eyes rimmed with tears.

�And why?� he asks. �I feel this powerlessness in American people. That we don�t feel we can do anything about it. . . . And that is so untrue. If history proves anything, it�s how resilient and how powerful American people are. But we are stuck, and we are lost in our addictions, in our comfort. . . . we are very complacent in the comforts of our life.�

In his effort to overcome this complacency in himself, Aversano joined September 11th Families for Peaceful Tomorrows, in 2003. Peaceful Tomorrows is an organization dedicated, he says, to �breaking the cycle of violence.� Brought together by their shared loss, these survivors of 9/11 have created an international network of those have been affected by violence, he says. Peaceful Tomorrows is a way that Aversano, and others who feel like him, can come together to affect change.

Peaceful Tomorrows is holding a conference in New York City this week, Sept. 8-14. The conference, Civilian Casualties, Civilian Solutions, will be a forum for peacemakers and victims of violence worldwide to share their stories. Aversano will be among the presenters.

September 2006

