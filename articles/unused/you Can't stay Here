you Can�t stay Here

Advocates for the mentally ill worry that Albany Med�s plans to cut its number of psych-unit beds in half will cripple the treatment community

By Chet Hardin

 

�Let�s look at logic. Logic is that building there makes lots of money!� says Burnes Crumpler, pointing to Albany Medical Center. �And what are we asking for? Two floors. That�s it!�

A large sign hangs across his torso: �Don�t Cut Mental Health Services.� All around him, dozens of other people with similar signs march back and forth in a peaceful protest outside of the hospital. They have come together today (May 1) to call upon the administration to reconsider its proposal to slash by half the number of beds in its psychiatric unit.

Many of these protesters know each other, coworkers in nonprofit mental-health agencies or fellow consumers of outpatient-support services at day-treatment facilities. They are members of a large, active mental-health community that goes largely unnoticed in Albany County. They are providers, advocates, patients, students, and the family members of the mentally ill.

�Albany Med has the only short-term psych facility in Albany County,� says Jeff Traite, a mental-health professional. �We need it cause this is the only place people who need to stay in a mental facility for a week or two have to go.�

�It is not a glamorous business,� he adds, �being in the psych-hospital business. It is not a big money maker for the hospital. But it is absolutely needed. It is a service that absolutely needs to be here.�

Albany Med currently has 52 psychiatry beds housed in E2 and E3, the two floors of its mental-health ward. Concern over the loss of these beds was raised earlier this year when a letter issued by Dr. Steven M. Frisch to the regional director of the Office of Mental Health came to the attention of advocates. In the letter, Frisch, the hospital�s executive vice president of Integrated Delivery Services and the general director of Hospital Systems, signaled Albany Med�s interest in recertifying the psychiatry beds on E3. Instead of being available for the mentally ill, these beds would be set aside to treat a mixture of medical/surgical and pediatric patients.

Frisch lays out multiple rationales for the reorganization, all hinging on a �new care paradigm� for mental-health services. This is in part a reference to a white paper produced by the Albany County Department of Mental Health that outlined a vision for the future of the county�s coordinated services.

�We . . . understand,� Frisch wrote, �the desire to create a single, patient-centered �front door� for [mental-health] patients in need of assistance. In the past, it has been proposed that this access service might best be located in proximity to an acute care hospital, which ideally would provide inpatient acute psychiatry services.�

But as far as expecting Albany Med to be that service-access point, Frisch continued, forget about it. Albany Med doesn�t have the space available within its emergency department for a dedicated psychiatry unit, as would be needed. Plus, he wrote, the hospital doesn�t consider itself to be a �population based provider.�

�More typically we are positioned as the most technically capable and emergently available provider for disease based care in the region,� he wrote, noting the hospital�s �growing market presence . . . due to the availability of physician coverage across all of our programs. . . . As a result, we are accepting record numbers of hospital to hospital transfers across all of the medical/surgical disciplines.�

Simply put: Albany Med is quite busy with other patients, and other interests. Mental-health service is not the hospital�s top priority. Frisch then continued to detail the hospital�s plan to slash 26 of the 52 beds.

�There is a need for this mental-health service,� says protester John J. Steinhardt. �If they shut down the beds, a person with mental-health needs will not have a place to go, as there will be nowhere to transfer them to.� Steinhardt, an Albany County Consumer Advocacy Board employee, spent time in E3 three years ago, hospitalized for depression.


PHOTO: Joe Putrock

�There is no plan on the books right now for where to send patients,� he says. �I think that without a plan of correction, this shouldn�t be happening.�

The structure of psychiatric care in Albany County is, at first blush, a confusing puzzle of bureaucracy. This is not unique to Albany; the same labyrinthine configuration can be found anywhere state and county governments coordinate with private industry to provide a service. The finer details are confusing even to those people who have been involved in the system for years. But to understand the county�s inpatient treatment of the mentally ill�the treatment that is in question when discussing these 52 beds�it is necessary to understand the relationship between Albany Med and Capital District Psychiatric Center.

CDPC is one of New York�s 16 state-run psychiatric hospitals. It serves nine counties surrounding Albany. Every county in the state has its own general hospital where a person with acute mental illness can be hospitalized. For Albany, that is Albany Med. In Schenectady, it is Ellis Hospital, and in Rensselaer it is Samaritan Hospital.

In Albany County, when one is suffering a mental-health crisis, he will be taken first to Albany Med�s emergency room. Then he will be transferred to CDPC�s Crisis Unit, where he will be triaged and doctors will begin their evaluations. This tends to last no longer than 24 hours. If the patient still needs medical attention, CDPC staff will place him in an acute-care facility, such as Albany Med, where he can spend up to 28 days on Medicaid. If a patient in an acute-care facility needs long-term hospitalization, and can�t afford a private institution, he can be transferred back to CDPC where he might stay for months, or years.

The relationship between Albany Med and CDPC is a unique one. Not only are the facilities close in proximity�an all-weather breezeway connects the two�but the care they offer is well-coordinated, based on a 35-year-old relationship of sharing the responsibilities, costs and resources needed to treat the mentally ill. This relationship can be seen as the center of Albany�s critical mental-health-treatment community. A whole complex of agencies and practices, advocates say, has been built up around the Albany Med area.

However, says Mame Lyttle, the advocate responsible for today�s rally, the current system is far from perfect, and the fact that Albany Med wants to reduce its number of beds is simply outrageous. If anything, the bed number needs to increase, she argues, not decrease. As it is, those 52 beds are the only Medicaid-supported acute-care beds available in the county, and many times they are unavailable.

�We have heard that people are staying in the CDPC�s crisis center for two days, three days,� Lyttle says, due to the lack of open beds. �CDPC is not set up to do that. They do have the beds and the food service, but it is not a place where they are supposed to stay.�

If no beds are available at Albany Med and a patient is in need of hospitalization, CDPC will make an effort to place them in another acute-care facility. In the Capital Region, this means Ellis or Samaritan. Since the beginning of 2007, Ellis Hospital has accepted more than 50 patient transfers from Albany County. If the patient has health insurance or can afford private institutions, the options for hospitalization increase substantially to include institutions such as Four Winds in Saratoga County.

Sometimes, relocation can mean sending the patient to even farther outlying counties, Lyttle says. Such relocation can be very problematic to someone suffering in the throes of a mental illness.

�Taking people out of the county, where their family is,� Lyttle says, �is not good for their treatment. And once they get out, they have to take advantage of the services in that county. Sometimes, they can�t come back to this county. It is very difficult.�

This is a sentiment echoed by many of the protesters.

�It is very disorientating� to be transferred to a different community, says Traite. �One of the most important elements to getting out of a hospital is to have visits from family members, some connection to the outside. A support system. And if [a patient] is moved out to Timbuktu, they don�t have that. You know, old Mom can make it on a bus here, but she can�t drag herself out to Glens Falls or somewhere.�

Plus, Lyttle adds, transferring patients out of county can be a bureaucratic nightmare. She points to the story of Quanzhi Hou.

Hou, a student at Albany Med, was brought to the crisis center at CDPC earlier this year after allegedly trying to force his way into the home of an Albany couple. According to the Times Union, he was carrying a meat clever and muttering �I am a madman.� Arrested, he was held briefly at CDPC before being sent 90 miles away to Bassett Hospital in central New York. The judge had allowed the transfer of Hou on the condition that the court be notified within 24 hours of a decision to release him. The decision was made after a week of holding him, the court was not notified, and Hou was released onto the streets 90 miles away from his home.

Of course, Lyttle says, most mentally- ill people pose no threat to the community, but they can pose a threat to themselves. Much of the time they are poor and have little in the means of a support system. Taking a person in the middle of a psychological breakdown and transporting him to a new county, medicating him and then releasing him into a new environment, can be traumatic. These people are fragile, she says. They need security and structure to survive.

�There is an effort by some to paint Albany Med as the bad guy, and that couldn�t be further from the truth,� says Gregory McGarry, vice president for communications at Albany Med. For one thing, he says, although the plan has been approved by the leadership of the hospital, Albany Med hasn�t yet submitted the application to recertify the beds.

�This demonstration has left us scratching our heads,� he says. �Why is it in front of Albany Medical Center? We just don�t understand why they are demonstrating in front of our institution. Why aren�t they demonstrating in front of Capital District Psychiatric Center?�

After all, he says, it was the actions of CDPC that led to the current uncertainty surrounding Albany Med�s 52 beds. Last November, Albany Med received a notice of termination from CDPC, a one-year warning that the state agency meant to sever its contract with the hospital.

�Now we have had a 35-year-old joint program [with CDPC] to provide psychiatric care . . . and it has been a tremendously successful program,� McGarry says. �This was a model state-private partnership. It was a model program. And out of the clear blue we get a termination notice. . . . We were shocked by it. They brought up different issues, but the main thing was that they were ending the program.�

The agreement basically called for shared responsibilities for treatment, he says, including shared salaries for mental-health employees.

�Basically,� McGarry says, �the staff for the program, the physicians, many of those were shared-funding positions. The state would put so much into the salary line, the hospital would put so much into the salary line, and make it a somewhat competitive salary. Neither one of us could have done it on our own.� Over the past few years, he claims, CDPC has slowly been withdrawing its salary support, placing more of a financial burden on Albany Med.

�It�s not the way to conduct business,� he says. �I can�t imagine them [CDPC] walking away from a provider that has been an excellent provider.�

Jill Daniels, acting director of public affairs for the Office of Mental Health, speaking on behalf of CDPC, says that after 35 years all things change, especially contracts.

�The affiliation agreement currently in effect was developed many years ago,� Daniels writes in an e-mail to Metroland. �It does not adequately address the needs of the clients being served, and has not been updated to address the numerous changes that have occurred since it was developed.�

�Across the state,� Daniels continues, �communities are redesigning their mental health service systems to be patient- centered and recovery-focused. This means that services provided are of high quality and proven by science to work, and that service delivery is designed to meet the needs of the patients rather than the needs of the care providers.�

The termination letter from OMH clearly indicated the office�s intent to renegotiate the contract with Albany Med. It requested �that we commence immediately the process of amending the affiliation agreement to better meet the current needs of the parties and the local mental health system. I am confident that our two facilities can work together to resolve the issues before us with a new agreement that will ensure best practices and quality mental health services that are consistent with today�s standards.��

Nonetheless, McGarry says, it is at best a brusque way of doing business.

He also points out that Albany Med has received an unprecedented number of transfers from regional hospitals over the past few years. The hospital�s admissions, he claims, have risen 20 percent in the past five years.

�So we are literally busting at the seams,� he says. �We obviously don�t have one mission of psychiatric care. We have multiple missions. We are the regional trauma center. We have the only children�s hospital in the region. Twenty-four counties of Northeastern New York feed pediatric patients and trauma patients, and transplant patients. We are the only transplant center. We have a lot of critical missions for Northeastern New York that go well beyond the Capital Region. And yet we have been providing with pride psychiatric care and training and plan to continue to do so.�

Couple the skyrocketing number of patients with CDPC �pulling the rug out from under us,� McGarry says, and Albany Med has been �forced to re-look at how we can go forward and fulfill our missions for the region and care for psychiatric patients.�

�We know that Albany Med wants to get out of the psychiatry business,� says Mame Lyttle. �They would rather have nothing to do with it. They no longer want these crazy people wandering around their halls.�

Lyttle, who worked in the psychiatric unit at Albany Med, claims that the hospital�s lack of interest can be seen in the maintenance of the E-Building, the oldest part of the hospital.

�The showers are just awful. Patients would stay for two weeks and they wouldn�t use the showers, because they are so terrible,� she says. �There is this whole attitude, and the psych center is referred to so disparagingly. I would go down to the bowels of the building and deal with work orders. And people would say, �Oh, you�re from the crazy unit.� �

Nurses in the psych unit have complained that they are treated as second-class citizens.

�We are not satisfied with the services at Albany Med to begin with,� she says. �We are very dissatisfied with the services.� But much as a rundown car is better than no car, she is quick to add, the care at Albany Med is better than no care at all.

�What we are saying to [Albany Med],� she says, �is �We need you. You are a pivotal player in psychiatry services in Albany County,� and we are trying to encourage them to stay in this game.�

�There will be a ripple effect to the reduction of the beds at Albany Med,� predicts Albany County Legislator Nancy Wylie (D-8th District). Already, the services that are offered to the mentally ill are strained.

Wylie, whose son is mentally ill, chairs the Mental Health Subcommittee. She remembers just a few years ago when Albany Med tried to enact a similar plan. It was in 2002, and Albany Med was celebrating the 100th anniversary of its mental-health ward, the oldest such private institution in the country.

�We advocates demonstrated at that time,� Wiley says. �And I am sure that Albany Med would not admit to the fact that the demonstration helped them to reverse themselves. But I think it had an effect. And I am not willing to see a plan go through where the beds go somewhere else this time. I believe that Albany Med has a responsibility to the community. I believe they have a commitment to the community to do this.�

�You start decreasing the number of beds at Albany Med,� Lyttle adds, �you start pushing these people out onto the streets, and where are they going to go? The jail? This is not the direction we want to go. We would rather increase the number of beds at Albany Med and not need the jail cells.�

Lyttle is referring to the $13.5 million plan to construct a mental-health annex at the Albany County Jail. Currently the jail has the ability to segregate 50 mentally ill inmates, but they are looking to increase that number to 80. From what she has heard about the current facilities at the jail, the new annex will be a welcome addition. But the irony of closing down beds in a hospital and increasing the number of beds in a jail is not lost on her.

�It�s the disintegration of the entire system,� she says. �Look at the waiting lists at the Albany County Department of Mental Health that go on for pages. People are asking for services and there are not enough services. The most seriously psychotic folks will get services, if they have to stay in crisis, or if they have to be sent 90 miles away to another facility, that will happen. But then, as the person starts to be stabilized, and then is in need of other services, outpatient treatment, therapy treatment, all of those systems are overworked. All of those systems are stretched.�

�These beds are needed in central Albany, these psychiatric services are needed in the center of Albany where they have always been, easily accessible by patients and families. This is what we want,� Lyttle takes a deep breath and sums it up.

May 2007

�This is what they need.�