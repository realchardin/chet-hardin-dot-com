Give ï¿½Em Heck, Harry

By Chet Hardin

Photos by Chris Shields

Critics say the Troy mayor has no vision for his city; Tutunjian, trying to build momentum for his reelection campaign, says the progress speaks for itself

It was in January 2004, only days into his first term in office, that Troy Mayor Harry Tutunjian found himself standing in Lansingburgh in the bitter cold, watching firemen do battle with a horrific blaze that was ripping through a row of ramshackle buildings. For Tutunjian, it was a revelation. As he would remark a year later in his first State of the City address: ï¿½The fire department battled frigid temperatures and scorching flames. I will never forget the icicles hanging off their faces. I will never forget the commitment those men showed to the residents of Troy that night.ï¿½

ï¿½As strange as it may appear,ï¿½ the mayor said, ï¿½that three-alarm blaze set the tone for the entire next year.ï¿½

Within days, the buildings were razed. Tutunjian established the Action Team, with the direct mission of addressing quality-of-life issuesï¿½abandoned buildings, neglected streets, trash-strewn alleysï¿½issues that Tutunjian believed had been left untended to for years. Issues that consumed Tutunjian first as a successful business operator, later as a councilman, and then as president of Troyï¿½s City Council.

ï¿½Those houses were a danger to the neighborhood and an eyesore for the city,ï¿½ the mayor continued. ï¿½In years past, they would have been allowed to sit and decay. Not anymore. We cannot let fire-ravaged and derelict buildings stand in the way of our strengthening neighborhoods.ï¿½

In many ways, Troy is a different city today then it was just three years ago. A surge of interest by outside investors in Troyï¿½s notable pieces of real estate has generated a palpable buzz. Buildings that sat vacant for years, or housed failing businesses, have been scooped up and rehabilitated. Scores of young couples have moved in to take advantage of the decades-long economic depression, buying and breathing new life into single-family and multi-unit brownstones. New businesses have sprouted up, banking on the flow of new consumers. Rent and property values have jumped considerably. It seems like everyone is working on a scheme for getting in on the action.

This makes the mayor, who is up for reelection this November, very happy.

ï¿½I think Troy is ripe for development, and we are seeing that today with developers from around the world buying property in Troy and making it work . . . having a level of success that we havenï¿½t seen for years,ï¿½ says Tutunjian, sitting in his downtown office, pointing to the spate of Troyï¿½s high-dollar investments: First Columbiaï¿½s multimillion-dollar investment in Hedley Park Place and Flanigan Square; Jeff and Deane Pfeilï¿½s successful reimagining of the Ready Jell building as Powers Park Lofts, and their ambitious rehabilitation of the old Stanleyï¿½s building as high-end condominiums; and the Mooradian on River Street, which is experiencing high-dollar restoration by a Dutch company.

ï¿½There are single-family homes that are being built that are at market rates,ï¿½ he says. As quickly as a house goes up for sale, it is bought. ï¿½Home ownership is the cornerstone to successful neighborhoods and a strong city, and that is our goal, to increase home ownership in the city.ï¿½

The administration, and its proponents, are quick to point to Tutunjianï¿½s tough-on-blight stance, and good-natured leadership, as the critical fuel for this real-estate-buying craze and the ensuing excitement. But many of his critics offer a different view for what is happening in Troy. For them, this administration has merely acted as a caretaker, taking credit for improvements it never would have the foresight to engender, letting real opportunities for improvement languish for want of vision. And Tutunjian, many say, is just the pleasant-appearing figurehead for an otherwise retaliatory and thuggish crew.

ï¿½This is something that I have been interested in my entire career,ï¿½ says James Conroy, the Democratic candidate for mayor. ï¿½I was born and raised in Troy. Iï¿½ve always had a love for the city and its people. My education is in the field of public management, and Iï¿½ve always wanted to be the chief executive of my city. This would be a wonderful opportunity.ï¿½

Conroy was deputy mayor under former Mayor Mark Pattison for eight years, and prior to that worked as a city planner under John Buckley in the 1970s. Himself a real-estate agent, Conroy notes as well the recent rush to own a piece of Troy landscape. He is quick to point out that he was the agent who sold the Pfeils the Ready Jell building in North Troy and also the old Mooradiansï¿½ building to the Dutch firm.

ï¿½I agree that Troy is going in the right direction,ï¿½ he says. ï¿½And a large reason for why it is going in the right direction is a lot of the decisions that were made years ago by Mark Pattison and my administration to put those tools in place that are now coming to fruition. Like the South Troy working waterfront plan. And the Congress Street initiative. Many of the improvements in downtown come from the fact that we redeveloped Congress Street, Fulton Street and Broadway, completely. We ripped up the pavement, put in new infrastructure, and we put down new pavement. We made the streetscape much more attractive, and I believe led to much of the interest in downtown Troy.ï¿½

ï¿½We had the foresight to lay out a number of initiatives and then also provide funding for them, or the mechanism to get the funding for them,ï¿½ Conroy says. ï¿½One of my strongest criticisms of this administration is that they have done nothing for three and a half years. And now, all of a sudden, they are trying to pack four years of work into a six-month duration. My frustration is that there are so many things that could have been done, and we could be so much farther then we are now.ï¿½

He points to the plans to extend the South Troy road, which would serve as a link between Adams Street and Main Street running through the industrial area along the river, as an example of this administrationï¿½s lack of initiative.

ï¿½We had the money,ï¿½ he says, ï¿½we had the design, we were ready to go to the Department of Transportation to get the approvals, and it has just sat there. Nobody is pushing it.ï¿½

He also points to the industrial site at the bottom of South Troy, King Fuels (commuters of the Route 378 bridge will remember the winking eye painted on King Fuelsï¿½ massive fuel tank), as a key example of the mayor taking too long to finish projects.

ï¿½Weï¿½in opposition to Harry who didnï¿½t want us to apply for the grantï¿½applied for the grant that provided the money that is now being used to buy the land,ï¿½ he says, referring to the Brownfield Economic Development Initiative that supplied the money to purchase the $2 million site.

ï¿½We had a brownfield cleanup plan that has sat idle for years,ï¿½ Conroy continues. ï¿½They were going to do an evaluation and remediation plan of that entire area down in south Troy. Now, this year, they are actually doing some of the testing.ï¿½

Tutunjian has a different take on the history of the King Fuels site.

ï¿½The previous administration for eight years didnï¿½t collect property taxes and didnï¿½t foreclose on the property when they could have,ï¿½ Tutunjian says, claiming that the former owners owed the city hundreds of thousands of dollars. ï¿½Once we took office, we looked at foreclosure and actually pursued it. The company went bankrupt. We were able to get the property through the bankruptcy sale. And for a lot less than what the previous administration wanted to pay for it.ï¿½

ï¿½The fact that we own King Fuels is a huge step in the right direction,ï¿½ Tutunjian says. ï¿½Very shortly, you will see some changes, as in removal of the unsightly buildings.ï¿½

The same differences of opinion can be found in the cityï¿½s undertaking of the massive Congress Ferry Street Corridor development, which partners Troy with the Housing Authority, Rensselaer Polytechnic Institute, and Rensselaer County in the construction. Conroy claims that the Congress Street project was born out of the Pattison administration.

ï¿½We went to the other members who owned property in the area and convened this group and said, ï¿½We have almost 14 acres of land that, if we all contributed, we could have a development. So the idea of having a single development in the lower Congress Street corridor came out of our shop. And we were ready to go out then and solicit a developer. That didnï¿½t happen because City Council didnï¿½t support it.ï¿½

Conroy contends that development has lagged and should have started years ago. The current administration says that progress is on schedule, with shovel striking dirt slated for next spring.

ï¿½Everything is moving forward well,ï¿½ Tutunjian retorts. ï¿½Would I like to see more progress? Sure. I like to see things happen quick, but many different entities are a part of this project and things move slower than you like at times.ï¿½

ï¿½As the Democratic city chairman in Troy, it is my opinion that there are three major issues that the voters of Troy should recognize this election year,ï¿½ says Frank LaPosta. ï¿½First, promises made and promises not kept. The administration has had roughly over three years to produce things, and basically it has been promises and promises that have not been kept. Second is ethics. You cannot have people running government who cannot be trusted. And third, crime. Violent crime is up 23 percent in the city of Troy over the past couple of years.ï¿½

Actually, the 23-percent increase is over the past decade, according to the Department of Criminal Justice Services. So far this year, violent crime is up 6 percent over last year; robberies are up 18 percent from last year; and burglaries are up 3.7 percent.

ï¿½The provision of safety is the No. 1 responsibility for local government,ï¿½ adds Conroy. ï¿½Economic development and quality of life are contingent on feeling safe. It is an underpinning of the perception of the community.ï¿½ In his mayoral bid, Conroy proposes a 10-point program to combat the issue of crime, including measures such as full staffing of the police department, the installation of cameras and the assigning of tactical units to ï¿½high-crime areas,ï¿½ and leaving ï¿½the management of the police department to the professionals.ï¿½

ï¿½[Crime] is certainly not out-of-control,ï¿½ Tutunjian says. ï¿½There has been a spike in crime numbers in the city. But we are certainly on top of it. We are aggressively enforcing the law in North Central. There have been details put in place.ï¿½

When the crime numbers are low to begin with, the mayor points out, a slight increase will make the crime numbers spike.

ï¿½Crime stats are tough,ï¿½ says Jeff Buell, Troyï¿½s deputy director of public information. ï¿½This is not saying that we are burying our heads in the sand, cause there has been an increase in crime and we are addressing it. We moved the substation from Lansingburgh to North Central. But when you go into crime numbers and use them as an argument for your side, it is tough to do.ï¿½

In 2005, Buell notes, Tutunjian stated in his State of the City address that the city would likely see an increase in crime numbers, as the administration and police force were shifting their policy on policing ï¿½away from targeting upper-level, more insulated criminalsï¿½ and instead focusing more on street-level crimes and ï¿½quality-of-life violations.ï¿½ As the policing shifted to looking for more crimes, the numbers would naturally go up. Itï¿½s not like the crimes werenï¿½t being committed before, he says, itï¿½s just these smaller criminals were slipping through the cracks.

ï¿½If a policeman is standing at an intersection and a car goes by, then a car goes by,ï¿½ Tutunjian says. ï¿½If a police officer is standing at an intersection and he notices a license plate is missing, stops the car, and finds handguns and drugs, then a crime has been committed. They were there anyway, but since we did our job, we found it. The crime hadnï¿½t been committed until we addressed it.ï¿½ Plus, the mayor stresses, the serious crimes in Troy are not random acts of violence. ï¿½If you are walking down the streets in Troy, minding your own business, chances are 99 percent that you are going to be safe.ï¿½

ï¿½People were murdered in the city of Troy and that is because we have a better policing policy?ï¿½ LaPosta asks. ï¿½Violent crime is violent crime. I donï¿½t understand anyone saying that violent crime is up 23 percent is because of good policing. And over the last several years the percentage of crimes closed is very smallï¿½17 percent. So something is wrong. Violent crime is up, closed cases are roughly 17 percent; if you are a criminal, you might as well commit a crime in Troy. You have an 83-percent chance of getting away with it.ï¿½

ï¿½This administration reminds me in so many ways of the administration in Washington right now. They keep their own counsel, and that is so dangerous,ï¿½ says City Councilman Bill Dunne (D-District 4).

ï¿½Talk to businesspeople. Talk to homeowners,ï¿½ Conroy says. ï¿½They wonï¿½t admit it publicly, because they are afraid of retaliation. But what they will tell you is that this administration runs on intimidation. That is their modus operandi. That is why they deserve to be thrown out of office. It is an abuse of power. Itï¿½s an arrogance that says that they are above the law and above the rules.ï¿½

The most public instance of this alleged retaliation happened to Troy native Jim de Sï¿½ve. Last year, de Sï¿½ve, who has been a longtime critic of Tutunjian and his administration, sent an e-mail to the mayor expressing his outrage over the allegations leveled by former Rensselaer County staffer Colleen Regan.

Regan accused Buell, Deputy Mayor Dan Crawley, and Dept. of Public Works Commissioner Robert Mirch of enlisting her in slandering a political opponent. Regan claims that she was forced to assume the fake name ï¿½Tonyaï¿½ and read from a script accusing Dunne, who was running in District 4 at the time, of sexually harassing her. This was during office hours, she says, and occurred in an office in Troy City Hall. The message was to be used as part of the campaign for Dunneï¿½s Republican opponent. (Albany District Attorney David Soares has since been appointed special prosecutor to investigate this and other allegations of misconduct in Rensselaer County.)

ï¿½We own five buildings in the city,ï¿½ de Sï¿½ve says. ï¿½We pay huge amounts of tax every year. And we like to see what our money is going to. If our money is going to Jeff Buell and Deputy Dan and Bobby Mirch to spend city resources attacking a City Council candidate, then I have something to say about that. And I think everyone in Troy ought to say something about that.ï¿½

De Sï¿½ve shot off an e-mail to Tutunjian asking that he request the resignation of the three men. A few days later, de Sï¿½ve was issued a code violation for graffiti spray-painted on his building.

ï¿½I am not running against Jim de Sï¿½ve,ï¿½ Tutunjian says. ï¿½If he wants to run for mayor, there is a little time left for him to get on the ballot. And for me to sit here and defend myself to him, when he is not a very credible person in my book . . . ï¿½

Tutunjian claims that it wasnï¿½t the e-mail condemning his administration that got the code departmentï¿½s attention, but an e-mail de Sï¿½ve sent days earlier requesting that DPW scoop snow off of the sidewalks of a city park.

ï¿½He sends me an e-mail asking for code enforcement, and he gets cited for something,ï¿½ Tutunjian asks, ï¿½and he calls that retaliation?ï¿½

ï¿½There was another e-mail that I did send before,ï¿½ says de Sï¿½ve. ï¿½But it was not about code enforcement, it was about shoveling snow off of city-owned sidewalks. I wasnï¿½t asking for codes to come into the neighborhood or for codes to do anything. I was asking the mayor to get on the DPW to come up here and shovel the city sidewalks.ï¿½

Essentially, says de Sï¿½ve, the mayor is saying one of two things: ï¿½We didnï¿½t give him a citation because he was complaining about corruption at City Hall,ï¿½ or, ï¿½We didnï¿½t give him a citation because he was complaining about the city not fulfilling its obligation to clear sidewalks.ï¿½

ï¿½It is not good in either scenario,ï¿½ de Sï¿½ve jokes.

It is especially hard for de Sï¿½ve to not view this as retaliatory considering that graffiti is not illegal in Troy. Graffiti is not referred to anywhere in Troy city law, he says. In New York, it is only against the law to create graffiti on private property. It is not illegal for the graffiti to exist on that property.

ï¿½When I discovered that graffiti wasnï¿½t illegal, I sent a letter asking for them to point out the part of code that was being violated on the citation, which is what you are supposed to do,ï¿½ de Sï¿½ve says. ï¿½And they ignored that letter. They sent me another letter that said I had to remove this [graffiti], and if I donï¿½t, they will take me to City Court. So I sent another letter saying that as far as I can tell, there is no mention of graffiti in city codes. Graffiti is not illegal. So then we get another letter telling us that we had to remove the ï¿½signï¿½ on our property because we didnï¿½t have a permit for it.ï¿½

ï¿½So now they are changing the definition of the graffiti to a sign,ï¿½ he says. ï¿½I mean, the graffiti says ï¿½junkie.ï¿½ So what, are we advertising drug abusers?ï¿½

De Sï¿½ve asked for the city to send him a photo of the alleged sign, to which the city responded with a photo of the graffiti. So de Sï¿½ve wrote back pointing out that the picture is of graffiti and not a sign.

ï¿½And we havenï¿½t heard back from them yet, cause it is ridiculous,ï¿½ he says. ï¿½And it is not only ridiculous, it is harassment. And we pointed out to them that it is harassment. So why did codes come to my house? Either because I was complaining about the city doing a bad job shoveling sidewalks of city parks or it was because I was complaining that they are a bunch of corrupt bastards. In either scenario, it is wrong, what they did, and it was an abuse of power.ï¿½

Allegations of corruption are not the sole province of the Tutunjian administration. Conroy will be working hard to get votersï¿½ minds off of a Housing and Urban Development scandal that has dogged him for nearly a decade.

ï¿½The vicious accusations against me associated with this thing bother me terribly,ï¿½ Conroy says, face flushing red as he recounts the HUD scandal that is sure to color his campaign for mayor. ï¿½Believe me, in this campaign, you will see it on everything.ï¿½

While deputy mayor, Conroyï¿½s brother, Stephen, sought and received a $27,000 homeowner grant from HUD. James Conroy was, at the time, part owner of the house, and the management of the city HUD grants fell under the auspices of his office.

ï¿½I asked the ethics board if this was an issue, and their answer came back ï¿½no,ï¿½ even though I would benefit because I was part owner, they said that it would be no different than anyone else who would benefit from that grant.ï¿½ He says that the ethics board told him all he needed to do was recuse himself.

He claims it was never proven that he did anything to influence HUDï¿½s decision, but the fight over the grant was nasty. LaPosta, who was a conservative at the time, and president of the City Council, was one of the Conroyï¿½s toughest critics.

ï¿½We went head-to-head over that,ï¿½ LaPosta says. ï¿½Why I objected to that, I felt that perception is reality. I believed that you cannot enrich your own family in any way, even if they are entitled to those benefits, while you are running the show. That was my whole issue. We were trying to bring Troy back, and I just didnï¿½t want that to tarnish our efforts.ï¿½

ï¿½I donï¿½t trust him. He isnï¿½t trustworthy,ï¿½ Tutunjian says of Conroy. ï¿½When in a position of power, he abused it for personal gain. And that is the blackest mark you can put on any public official. And Iï¿½ll leave it at that.

ï¿½The election in November is about our records. Not what you will do if you are there. What Iï¿½ve done is what Iï¿½ve done. My opponent was here [in City Hall] for eight years, and he did some things that he shouldnï¿½t be proud of.

ï¿½It is going to be up to the people to decide who to put back into office,ï¿½ Tutunjian says. ï¿½I sleep very well at night. My conscience is clear.ï¿½


August 2007