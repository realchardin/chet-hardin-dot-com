Have Laptop, Will Travel

No matter where you go in the Capital Region�s video-sound-art scene, chances are you�ll run into skfl

By Chet Hardin

Video artist skfl, aka Jason Steven Murphy, gets a mischievous grin when he talks about his friend Kathleen�s old home movies.

�She foolishly gave DJ Back From Japan a DVD of all of her home movies from when she was a kid,� says skfl. �Back From Japan and I were doing video down in the city, and we had those clips and we knew she was coming, so we were like, �We gotta use these.� She was embarrassed and psyched.�

Skfl transforms the charming footage of Kathleen through multiple video effects, then shrouds the young ballet student with an overlaid, equally tweaked, second video stream. The result is a fractured, colorful, ghostlike window through which we watch images of forever-child Kathleen practicing pirouettes.

�There is something about these home movies, the texture, the camera, that just totally ends up working out, even when you severely stomp the image with effects,� skfl says. �Everyone has that moment of having seen or having filmed or having been involved in their family�s home movies. There is something about them that when they are up on screen you can totally relate to.�

Skfl started VJing a few years ago, after graduating Ithaca College with a cinema degree. He came to Rensselaer Polytechnic Institute to do his graduate work and was able to submerge himself in the video-sound-art scene growing out of the engineering school�s groundbreaking arts department, iEAR. He was drawn to the art of video and music as one greater experience.

�I would like to think that I have knowledge of visual language or filmic language,� he says. �And I like doing video, and I like film, but as far as a day-to-day obsession, it has always been music. Even when doing video I think in those terms. It is all tied to that in my mind. �You got an atmosphere, you got the feel, people, temperature in the room, the music. Adding video art to that introduces another level. It doesn�t overpower the DJ. It adds to the experience.�

Murphy, who took the pseudonym skfl while he was a student at RPI, laughs now about the origins of the name. He was starting a new radio show at WRPI and wanted an assumed name to go with it.

�For some reason, I had all these friends who had pseudonyms for either purposeful reasons, cause they were artists, or legal reasons, or they were vaguely insane,� Murphy says. He figured he should have one, too.

The week he was slated to start this show, Lonnie Donegan, the celebrated �king� of skiffle music, died. �There was something about the word �skiffle� in my mind that sounded right. And for some reason I was really into taking vowels out of things, which is somewhat lame in hindsight.�

But �skfl� looked good graphically, he thought, and it was confusing.

�In hindsight, I really wish I had done �sktl,� because I love Skittles; I could eat them by the pound,� he says. �And have.�

Murphy stays busy. In addition to VJ gigs, he co-moderates Tiena List, a widly used Web group for events listings, and acts as the point man for RPI�s Experimental Media and Performing Arts Center, or EMPAC.

To understand EMPAC, Murphy says, you have to understand that it is currently two separates things: a building that is under construction, which will house performances in the future, and an arts program designed to promote the artistic vision of EMPAC.

�There is EMPAC the building, which has got a zillion people building it,� Murphy says. �But we got the idea that you just can�t just open a building. You got to hire a staff to get this idea of EMPAC, the creative entity, going beforehand. So that is basically what I do. I make everything happen from an administrative standpoint. None of it is necessarily glamorous. From travel, hotel, getting beer to and from a place, arranging chairs, it just runs the gamut.�

Murphy started working at EMPAC in 2004. He was brought in to assist the overtaxed director, Johannes Goebel, on one of the early projects, a retrospective of the San Francisco Tape Music Center.

�Johannes started getting it going and quickly realized that he couldn�t build a building and make that event happen,� Murphy says. �So he hired me, on a temporary arrangement, and apparently we got along well.�

EMPAC holds shows all over RPI�s campus in various spaces�the Heffner Alumni House, rented trailers, Rensselaer Playhouse�some that were designed as auditoriums, some that were not. They even hold events off-campus. The installation Bubbles moved from the concourse at the Empire State Plaza to the Junior Museum in Troy to the Schenectady Museum.

�In terms of space, every show that we do, we are like, �Oh, I got the perfect space to do this, and it is in that building that they are building that has our name on it.� And we go from there.�

One of the challenges for EMPAC is to appeal to disparate communities, from RPI to Troy to the Capital Region and beyond�New York City, Boston, Montreal. �You got to understand, there is not a space like this in the United States,� he says. �You have got to engage Boston and New York, these cultural Meccas, and their standards are totally different.�

Skfl�s have-laptop-will-travel attitude toward video means that he tends to throw down with a lot of people in very different situations. One weekend, he might be VJing with DJ Back From Japan, aka Kevin Luddy. �That�s like a party, right?� skfl says. �People are totally dancing, and they catch the image for a minute.�

Whereas the next week, he might be working on projects such as the Domeworks presentation, Projection, Reception, Perception, at the Schenectady Museum, �which was about experiencing the sound art and space,� he says. �It�s a totally different thing. At that point, people are paying a lot more attention to your video. You have to be aware that people aren�t going to be dancing for 10 minutes, drinking, and then watching your video for a minute. People are going to be watching.�

And when they watch skfl�s video, they will see images in a constant state of metamorphosis, never settling upon a completion. �That idea of change�minute or gradual�is what I want to do,� he says. �You start out with an idea, it could be arbitrary, it could be based on what you are listening to, in terms of the music, or the moment. It is generative.�

Originally published in Metroland November 2006