<?php include('../header.php'); ?>
	<main id="main" role="main">
		<article>
			<header class="article-header">
				<h2>Get With the Programming</h2>
                <p class="description">To some, it is a useful training program; to others, a cult. Either way, NXIVM and founder Keith Raniere remain embroiled in controversy</p>
                <p class="hyperlink">Link to this: https://chethardin.net/articles/nxivm</p>
			</header>
			<div class="content">
				<figure> 
					<img src="/images/keith.jpg" />
					<figcaption>Photo: Patrick Dodson</figcaption>
				</figure>

				<p>It was 1991, and Toni Natalie was smoking again. She thought that she had kicked the habit, but when her brother fell ill, she found herself spending all of her time at the hospital. "It was stressful," she says. "You know how it is." When his girlfriend offered her a cigarette, she took it.</p>

				<p>But she wanted to quit. So when Keith Raniere, who claims to be in the 1989 Guiness Book of World Records for "highest IQ," offered to help her, she thought, "Great! The world's smartest man wants to help me quit smoking?" Raniere told her that they would need to go to a quiet place, where he could talk to her.</p>

				<p>"So we go into his office, and I remember him asking me questions like, 'What makes you nervous?' and 'What relaxes you?' Talking to me; touching my knuckles, using that as trigger points [for hypnosis]. And I came out of the room, and my husband says to me, 'Geez, what were you doing in there all that time?' And I was like, 'What do you mean all that time?' " She felt as though she had been alone with Raniere for 15 minutes. Her husband informed her that, no, she had been gone for almost two hours and 45 minutes.</p>

				<p>"I guess some people are more susceptible to this stuff than others," she says. "I was the prime candidate. And that was the beginning of the end."</p>

				<p>Raniere had made the news a few years earlier, when, as a 27-year-old state worker, he was accepted into the Mega Society, an exclusive union of those who possess extraordinarily high IQs. (Think Mensa on steroids. At the time, there were only three members worldwide). In a Times Union profile on Raniere, impressive claims were made: He graduated from Rensselaer Polytechnic Institute with three simultaneous degrees in math, physics and biology; he was able to spell "homogenized" by age 2, after seeing the word on a milk carton; and at age 4, he understood quantum physics. The article also stated that he had won the East Coast judo championship at 12 and had tied the state record for the 100-yard dash. Plus, he only needs two to four hours of sleep a night.</p>

				<p>When Natalie met Raniere, he owned and operated Consumer's Buyline Inc., an outfit he had started a year earlier in Clifton Park. It was a discount buyers club--members were paid a commission for enrolling new members--and it brought in a lot of money. At its height, the company boasted 250,000 distributors nationwide and 173 local employees. One year, it reported sales topping $30 million.</p>

				<p>Natalie and her husband were in their early 30s, living in Rochester, and had been married for about four years. They had a small, adopted boy and a nice home. "We were in love," she says. "It wasn't like a Gone With the Wind kind of love. But we were happy. We were friends." They had just won the award for selling the most CBI memberships for the month, so they traveled to Clifton Park to pick up their prize and to see the company headquarters. Plus, she adds, they were curious about Raniere.</p>

				<p>"When I went up to Clifton Park for the first time," Natalie recalls, "Pam Cafritz was running around in the background going, 'Is she Family? Is she Family? She looks like Family. Is she Family?'"</p>

				<p>"And I remember Keith saying, 'Yes, she's Family.'"</p>

				<p>Natalie had no idea what Cafritz, an associate of Raniere's, had meant; she just took it as the friendly enthusiasm of eccentric strangers.</p>

				<p>"And Pam said, 'Is she The One?'" Natalie recalls.</p>

				<p>"And Keith said, 'She could be.'"</p>

				<p>Within a year, Natalie had left her husband and moved to Clifton Park to be with Raniere.</p>

				<p class="section-break">By the mid '90s, CBI had collapsed under the weight of 25 state and federal investigations. Attorneys general and others alleged that the company was an illegal pyramid. Raniere settled with New York state for around $50,000, but denied wrongdoing.</p>

				<p>With CBI in ruins, Raniere moved on to his next venture: Executive Success Programs. He established the "human potential training" business in 1998 after meeting former nurse and noted hypnotist-therapist Nancy Salzman. Salzman had been working as a family therapist for years, and she boasted a reputation as a skillful practitioner of "neuro-linguistic programming" and Ericksonian hypnotherapy. Milton Erickson, who created this specialized form of hypnosis, was an American psychiatrist whose work included the exploration of hypnosis-inducing handshakes and the use of shock to eliminate phobias. Erickson is also noted for his work in neuro-linguistic programming, the controversial study of body-language cues and language patterns.</p>

				<p>Salzman became the public face and president of ESP and Raniere became her mentor.</p>

				<p>It was around this time that Raniere began to call himself Vanguard, Natalie says. "He was trying to find the right terminology for himself. Vanguard, creator of a movement."</p>

				<p>According to ESP's Web site, they have "created international coaching standards that allow for accurate, consistent measurement of human psychodynamic performance. All ESP programs maintain standards of excellence and impart a unique, patent-pending technology called Rational Inquiry(cr) that enhances human performance in virtually every field of human endeavor."</p>

				<p>ESP, or NXIVM (nex-ee-um) as it is more commonly known, is a growing and outwardly successful organization. Its classes are offered in Albany, Saratoga Springs, New York City, Washington state, Alaska and Mexico. Its seminars have drawn high-powered followers and supporters, including former U.S. Surgeon Gen. Antonia Novello; Sheila Johnson of Black Entertainment Television; relatives of two former Mexican presidents; Clare and Sara Bronfman, the daughters of billionaire Seagram's heir Edgar Bronfman Sr.; and on and on.</p>

				<p>A 2003 Forbes article, "Cult of Personality," reported that NXIVM earned $4 million yearly at the time. In a July 2006 follow-up, the magazine alleged that Salzman now lives in Sara Bronfman's $6 million Manhattan apartment, with unlimited access to the Bronfman jet. Former NXIVM insiders claim to have witnessed the gifting of a $20-million 'tribute" from the Bronfman daughters to Raniere at his 2004 weeklong birthday celebration, Vanguard Week.</p>

				<p>But along with its popularity and successes, NXIVM has drawn vocal opponents who allege that its training sessions are manipulative and destructive, exacting a cultlike devotion from students. And Raniere, they claim, is the classic example of a cult leader, who surrounds himself with fiercely devoted (and mostly female) followers.</p>

				<p>"Keith has always had women in front of him," Natalie says. "That is what he does. He takes vulnerable women, and he very easily becomes everything they need or want."</p>

				<p>NXIVM's goals, as stated on its Web site, are ambitious: "To help transform and, ultimately, be an expression of the noble civilization of humans." This includes the construction of training facilities or NXIVMs. One possible facility was proposed for southern Saratoga County. The two-story building would have been 67,000 square feet and would have included a physical-therapy center and day care, but residents resisted construction, and in 2003, the plan was nixed.</p>

				<p>Undeterred, NXIVM and its members continue to grow their real-estate holdings and own at least 12 properties in the Capital Region. These include single-family homes, condominiums and commercial property, the organization's training facilities on New Karner Road in Latham, and Romano's Family Restaurant on Route 9.</p>

				<p>"Keith's ultimate goal has always been to have his own commerce, his own language, his own people where he is the king," says Natalie. "What he is doing is creating his own world in Clifton Park. . . . These people believe he is God. He believes he's God."</p>

				<p>There are two courses of study offered through NXIVM. One, called Ethos, is relatively casual. A participant can attend any number of training sessions, offered regularly throughout the week. The classes last only a couple of hours, and focus on emotional training. Susan Miller, CEO of Goold Orchards, attended nearly a dozen Ethos sessions. The program, she says, was good for her.</p>

				<p>"It was a difficult time for me. My mom had passed away," she says. "I had good changes." But she resisted taking the other course of study offered: Intensives.</p>

				<p>"They make a lot of pitches for it. And they charge a lot of money for that program. My gut kept telling me that [Intensives] were not the thing for me to take. I went to a sales pitch one time . . . and I never went back. It just didn't make any sense."</p>

				<p>Intensive Training is offered in five-day and 16-day sessions. The courses can last up to 14 hours straight. Some former students, or Espians, have claimed that during these sessions, room temperatures are kept uncomfortably high, the food served either lacks the necessary protein to maintain strength or is served irregularly or not at all, and that a large number of coaches are present, creating an atmosphere of physical intimidation. The stress involved, they add, can be unbearable.</p>

				<p>Kristin Marie Snyder was taking her second, 16-day Intensive in Alaska when she apparently suffered a complete psychological breakdown. Alaskan authorities suspect that Snyder, an seemingly successful young professional, drowned herself by canoeing out into the middle of Resurrection Bay and capsizing. She had been involved with NXIVM for four months.</p>

				<p>Her suicide note, as reported in Times Union on Feb. 1, 2004, read: "I attended a course called Executive Success Programs . . . based out of Anchorage, AK, and Albany, NY. I was brainwashed and my emotional center of the brain was killed/turned off. I still have feeling in my external skin, but my internal organs are rotting. . . . I am sorry life, I didn't know I was already dead. May we persist into the future." Her parents claimed in the article that Snyder had become convinced that she had been responsible for the Columbia shuttle disaster.</p>

				<p>"When I read her suicide letter," Natalie says, "I couldn't believe Keith was doing this to other people. I only thought he was doing this to me." As a result of therapy sessions with Raniere and Salzman, Natalie says that she became convinced that she had been responsible for the Nazi Holocaust.</p>

				<p>One time, Natalie claims, a group of Espians cornered her in her office. A man in the group said to her, "Close your eyes. Close your eyes and hold out your hands." She did as she was told and held out her hands, in which he placed a knife.</p>

				<p>"And I said, 'What's this?'"</p>

				<p>Natalie says that the man replied: "'You should know, because that's the knife you killed me with.'"</p>

				<p class="section-break">The pressure NXIVM places on its students to recruit others for Intensives is unremitting, says former Espian Maria (not her real name; she asked to remain anonymous for fear of retaliation). From a prominent Mexican family, she was 27 years old and studying for her master's degree in New York City when her brother recruited her.</p>

				<p>"The first thing they tell you," Maria says, "is [that] if you want to become more involved with the organization, you have to bring two people for the next Intensive. So my brother brought my mother and myself."</p>

				<p>In Intensives, she says, Espians are introduced to the Matrix, NXIVM's ideology. And to learn the Matrix, sections of material called modules are explored. "Every module together completes the Matrix. Everything links to each other," she says. "The Matrix is very impermeable. It is a whole different way to see the world."</p>

				<p>Each module touches on a basic element of the human condition, and has a name such as money, trust, or "The Fall."</p>

				<p>To understand The Fall, one must understand suppressives. When a person is a destructive force in the world, Maria says, such as a serial killer, a terrorist, or a critic of NXIVM, when they exhibit suppressive traits, when they destruct value, they are referred to as suppressives. And just as Lucifer took a fall in Paradise Lost (an oft-referenced text in NXIVM circles), Espians are taught that suppressives are capable of taking The Fall. Once they have, it is taught that they have developed an anticonscience--good feels bad to them and bad feels good.</p>

				<p>It was in the teachings about suppressives and The Fall that Maria began to feel that NXIVM was warning: You cannot leave, you cannot criticize or question the organization.</p>

				<p>NXIVM has a strict hierarchy with Raniere (aka Vanguard) at the top. Directly beneath him is Salzman, who insists that students refer to her as Prefect. Students are expected to bow to Vanguard and Prefect. Sashes are worn to determine an Espian's level.</p>

				<p>"You start with a white sash when you are a student," Maria says. "Then you get a yellow sash when you coach. Then you get stripes. Then you get the orange sash. Nancy had a gold sash."</p>

				<p>The higher in the hierarchy, the closer an Espian is to integration, akin to being enlightened, perfect: Thoughts flow together flawlessly, Maria says, "like a plate without any cracks." Salzman is close to integration, she says, and Raniere is completely integrated.</p>

				<p>He is treated, she says, "like a Buddha or a Christ."</p>

				<p>After being taught the module's material, the students break up into groups. Espians from every rank sit together and discuss the lesson. "When you are in the group," Maria says, "you will be asked questions like: 'Is suicide good? Is suicide bad?'"</p>

				<p>A student might answer, "'I think it is bad, because you are being a coward.'" To which a coach might reply: "'I think suicide is not that bad. Because, if you are a bad person, if you are not constructing value, if you are destructing value, then you should not have a place in the world, and you should be happy to kill yourself.'" In this way, she says, a student's belief system is constantly undermined. Doubt is cast upon opinions, and beliefs are systematically torn down.</p>

				<p>One technique NXIVM employs is called Exploration of Meaning, or EM.</p>

				<p>Maria explains the process as she remembers it: A problem or issue is identified in the student. The coach, or a coach in training, questions the meaning of the issue, leading the student along a seemingly logical path with predetermined questions to the root of that problem. And when a student can relate the problem's manifest to the problem's root, there is an "aha" moment. The student has had an "integration." And the more integrated you are, the more you are living according to NXIVM's Matrix.</p>

				<p>"It is mathematical technology," Maria says. "It is like a mathematical equation. And you end up at zero every time. And once you are in zero, they give you the answers." Even though the Espian feels each discovery is a product of their own soul searching, she adds, they are actually being programmed.</p>

				<p>"It's fascinating. Can you imagine how brilliant this guy [Raniere] is?" she asks. "He really is a genius."</p>

				<p>After training with NXIVM for six months, Maria suffered a psychotic episode that she attributed to extreme stress. She began hallucinating and had to be taken to the hospital.</p>

				<p>"It was in the moment before . . . I went into my psychotic break," she says. "I felt that I had to decide between my boyfriend and my life, and Keith. And that's when I broke. It's the devotion to him; you have to give your life to him. I didn't want to lose my boyfriend, and I didn't want to lose my life; he [Raniere] wants to have the control of your life."</p>

				<p>Maria spent the next two years in treatment, taking antipsychotic and antidepressant medications. She still gets nervous talking about NXIVM and says that former Espians like herself are afraid to speak out because they know how suppressives are treated.</p>

				<p class="section-break">One such suppressive is Rick Ross, a New Jersey-based cult- intervention specialist who has been involved in lawsuits with NXIVM for the past three years. The controversial anti-cult lecturer and deprogrammer"first won the animosity of NXIVM by distributing the group's copyrighted material to mental-health experts for analysis and then posting their findings on his Web site, RickRoss.com. NXIVM sued him, seeking damages of nearly $10 million, and asked the courts to force Ross to remove the critical material. So far, the courts have upheld Ross' right to publish the reports.</p>

				<p>Lawsuits are nothing new for Ross. At one point, he says, he was being sued by three groups simultaneously: the Gentle Wind Project of Maine, Land Mark Education, and NXIVM.</p>

				<p>"I'm used to slap suits," he says. 'This is nothing new to me. . . . I regard it as part of my work. If you are not being sued or threatened on a regular basis in my line of work, it would give you pause to wonder if your work is meaningful."</p>

				<p>However, he says, he did not suspect the lengths NXIVM seemingly will go to silence its critics.</p>

				<p>Rifling through his bank statements from September and October 2004, Ross gets lost in the pile of papers: "Debits, debits, where are deposits?"</p>

				<p>"Oh, there it is," he says to himself.</p>

				<p>He is frustrated.</p>

				<p>"I don't do my own books."</p>

				<p>Ross is on the phone with a reporter, attempting to confirm information in a document that he claims he has never seen. The reporter is holding a copy of the nine-page report, including a cover page and a page that warns of the document's strict confidentiality. It appears to have been prepared on Nov. 23, 2004, for the O'Hara Group & Associates LLC, an Albany-based consulting firm that allegedly was working for NXIVM at the time. The report appears to be the work of a New York-based industrial-espionage firm Interfor Inc.</p>  

				<p>The document bears the title "Status Report," under which Rick Ross' name is printed. It contains sections such as "Criminal Record" and "Communications." It appears to be the result of an effort by Interfor to collect personal and business information on Ross. And according to the NXIVM insider, Raniere ordered the gathering of information not only on Ross, but on Toni Natalie as well.</p>

				<p>Interfor's president, Juval Aviv, worked for years as a counterterrorism consultant with the FBI. Before becoming a regular fixture on Fox News, Aviv claims that he was a member of the Israeli intelligence outfit Mossad. Aviv is the author of The Complete Terrorism Survival Guide: How to Travel, Work and Live in Safety and the fictional thriller Max. Steven Spielberg's Munich is based on a Mossad operation in which Aviv allegedly participated.</p>

				<p>Ross finds his October 2004 bank statements.</p>

				<p>"Give me a deposit," he says.</p>

				<p>The section in the document titled Financial Information lists 14 transactions, debits and credits. The entry from Oct. 12, 2004, is read to Ross. It is correct.</p>

				<p>"Well," he says, "they need to be arrested. They have penetrated into my checking account."</p>

				<p>"Anything else?" he asks. Another deposit is confirmed, and a debit as well. The report, it appears, contains his private banking information.</p>

				<p>"Mr. Aviv," Ross says, "had better get a good lawyer."</p>

				<p>This is not the first time Ross has heard of Interfor or Juval Aviv. He says Interfor contacted him in November 2004 (apparently at the same time the firm was mining information on him) to retain his services.</p>

				<p>Ross recounts his experience with Interfor: He was told that an old friend of Aviv's, a woman calling herself Susan L. Zuckerman, was seeking help in extracting her daughter, "Judy," from NXIVM, and they needed Ross' help. Interfor even cut him a check for $2,500 in November 2004 from its corporate account, stating that it was a retainer payment from Zuckerman. He met with Aviv, his assistant Anna Moody, and the woman who presented herself as Zuckerman at Interfor's office. There, he says, they hatched a plan to get Ross on a cruise ship with "Judy Zuckerman" so that he could confront her and work to deprogram her.</p>

				<p>"They wanted to know everything that I knew about NXIVM," he says. "They asked me question after question. What did I really think of Keith Raniere? Did I think he was a cult leader? What did I think of their process of Intensives and seminars? How many complaints have I received [about NXIVM]?"</p>

				<p>According to a NXIVM insider, the plan to get Ross on a crusie ship was a sting; the concerned-mother scenario was invented. And the role of daughter "Judy" was going to be performed by one of Raniere's closest associates, Kristin Keeffe. Perhaps, the insider says, they thought they could convert Ross.</p>

				<p>The plan fell apart after Ross made it clear to Interfor that he would not be alone with the "Judy," that someone would have to be in the room with them, he says. That's his policy. Shortly after that, he was told that Susan Zuckerman had changed her mind. He was given no further explanation.</p>

				<p>"What their goal was, ultimately, in the deception, I don't know," Ross says. But the scenarios are chilling. What could they have had in mind in going to such lengths to get him alone on a cruise ship with a member of NXIVM's inner circle?</p>

				<p class="section-break">Dr. Carlos Rueda, chairman of the Department of Psychiatry at Our Lady of Mercy Hospital in the Bronx, says that he, too, was contacted by someone, within the past few months, claiming to represent Interfor. They said they needed his advice: The daughter of a rich Mexican family had become involved in NXIVM, and she needed therapy.</p>

				<p>"They sounded very credible," Rueda says. "They sounded very corporate."</p>

				<p>After a couple of calls, he stopped hearing from them.</p>

				<p>Rueda joined the chorus of NXIVM's critics after treating three former Espians who, he says, had decompensated, or developed psychiatric illnesses.</p>

				<p>Their ailments, he says, can be linked to NXIVM's training, which places tremendous psychological stress on its students.</p>

				<p>"And this can be a devastating and long-term situation," he says. "Patients may continue to have episodes of psychosis or depression" far beyond the initial onset of the illness.</p>

				<p>What disturbs Rueda most about NXIVM is its seeming lack of interest in the welfare of its students. He worries that the training seminars are just a way to make money, with no concern for the psychological damage they level. This, he says, can be seen in how the organization treats students who suffer breakdowns.</p>

				<p>"They dump you as soon as they see that you are decompensating," he says. "They say that you are 'weak.'"</p>

				<p>Representatives of NXIVM and Interfor did not agree to be interviewed in time for this story.</p>

				<p>"And you ask, 'Why would a sane person put up with this?'" Toni Natalie says. "You are so controlled by these people, when you are involved with them, you don't associate with anybody other than The Family. You don't talk to anyone other than The Family. It is a complete disassociation from everyone--your family, your husband, even your children if they get in the way."</p>

				<p>It was 1998, and after more than six years with Raniere, Toni Natalie wanted out. She was taking the first NXIVM course when she had her own "aha" moment, realizing, she says, just how manipulated she had been by Raniere and Salzman.</p>

				<p>"I realized I had been their Guinea pig,” she says. “As I was taking the course, I was seeing myself in all of my sessions with Nancy. I had had hundreds and hundreds of [therapy] sessions with Nancy and Keith.” They had used those sessions, she believes, to experiment on her. "It was scary as hell," she says. "I can't tell you how frightening it was."</p>

				<p>After almost a year of working to divest herself emotionally and psychologically from Raniere and the Family, the day came when she finally snapped. It was over, of all things, a simple domestic spat.</p>

				<p>"Keith was home sleeping while I was working, which was normal," she says. "I mean, this bullshit that Keith never sleeps. Keith sleeps all day long. He sleeps all day long."</p>

				<p>She had asked Raniere to dry some clothes that were in the wash, pointing out to him a specific shirt that should not be dried because it would shrink.</p>

				<p>"I came home, and he had put the shirt in the dryer. I said to him, 'I asked you not to do this.' And he starts screaming at me, 'You're stupid! How dare you tell me I don't know what I heard. I have perfect retention. I have a 240 IQ.' And he is screaming in my face: 'Tell me that you are wrong! Say that you are wrong!'"</p>

				<p>He can't stand being told that he is wrong, she says.</p>

				<p>"And I wouldn't do it. He told me if I didn't say that I was wrong . . . he would go. And that was it. I was like, 'I'm done.' And that's when the torture from the girls started."</p>

				<p>Raniere, she says, was far from done with her.</p>

				<p>"Don't forget," she says sarcastically, "I am 'The One.' I was supposed to bear the child that was going to change the world."</p>

				<p>Raniere has in the past denied this allegation, saying that it is "not rational." Natalie insists that it is true. He was convinced that she was to have his child, she says. He even had members of the Family convinced, she says, despite the fact that Raniere knew that she is unable to have children.</p>

				<p>He wrote her letters, she says, that are by turns admonishing and pleading. Other members of the Family wrote her letters, as well, on Raniere's behalf. They were convinced that she absolutely had to return and have his child.</p>

				<p>"If I didn't return to him and bear this child, horrible things were going to happen," she adds. "So I am responsible for Sept. 11."</p>

				<p>Kristin Keeffe came to her house once, she says, with her a bouquet of flowers and a candy box with the image of a mother and baby on it. She was crying, saying, "'I had a vision that you changed your mind,'"  Natalie says. "'And you are coming back to us. And you are going to have the baby. I am so happy that you changed your mind and you are going to have the baby.'"</p>

				<p>Natalie took the flowers and candies to Lawrence LaBelle, a judge in Saratoga at the time. LaBelle remembers the situation vividly. Natalie, he says, had gotten herself caught up with a "very, very unusual group. . . . It's a cult, I think, a very, very bad cult."</p>

				<p>He can't believe Natalie is talking to reporters. Why, he asks, would she want to dredge up that misery?</p>

				<p>"They do everything they can to destroy your life, to keep you quiet," she says. "Especially me, because I know so much about them." Five years ago, Natalie was diagnosed with post-traumatic stress disorder, she says, because of what she was put through.</p>

				<p>"I know what was done to me," she continues. "I know what was done to my family. I know what they are doing to other families. I know how they mind-fuck people. I know how dangerous they are. I know how dangerous Nancy Salzman is, and Keith Raniere. They are very, very dangerous, scary people."</p>

				<p>It has taken her six years of therapy, she says, to be able to talk at length about her experiences with Raniere. She still lives in fear, and she says, rarely leaves the house alone.</p>

				<p>"The scary part of the organization," Natalie says, "is that they have a philosophy, his philosophy . . . called right action or wrong action. And if he believes that something is right action . . . it is OK to do whatever it takes. If you had to kill somebody, and it is for the betterment of The Family, it would be OK."</p>

				<p><i>Originally published by Metroland, October, 2006</i></p>
			</div>
		</article>
		<article class="section-break">
			<header class="article-header">
				<h2>The Stars Come Out in Troy</h2>
			</header>
			<p><i>Two TV celebrities--and apparent students of NXIVM--visit the Capital Region with hopes of starting a business</i></p>
			<div class="content">
 				<p>Last week, Troy Mayor Harry Tutunjian got a break from his regular humdrum duties to entertain, at least for a little while, celebrity. Allison Mack of TV's Smallville and Nicki Clyne of Battlestar Galactica were in Troy, scoping out the quaint downtown scene, visiting the shops and going out for dinner, and they popped in for a quick visit with the befuddled mayor.</p>

				<p>Mack and Clyne have said that they were interested in starting a business, and curious about the community's youth scene, said Jeff Buell, Troy's director of public information.</p>

				<p>Why Troy? Why the Capital Region?</p>

				<p>Because, according to multiple sources, Clyne and Mack are students of Executive Success Programs, or ESP, which is the educational wing of NXIVM, and have been for at least a year. Mack's co-star, Kristin Kreuk, has been connected to the organization for years.</p>

				<p>NXIVM, a Capital Region-based organization, purports to offer courses in "ethics, critical thinking and entrepreneurship." It has been criticized by many of its former members as destructive and cultlike.</p>

				<p>The head of NXIVM, Keith Raniere, started his human potential training business in 1998, after his first business, Consumers Buyline Inc., a multimillion-dollar discount buyers club, was shut down after 25 state and federal investigations. It was alleged at the time that CBI was a pyramid scheme, and Raniere was forced to pay New York state roughly $50,000.</p>

				<p>According to former insiders, Raniere is referred to by followers as Vanguard.</p>

				<p>Clyne and Mack's apparent goal is to start a business, the details of which are somewhat unclear, but according to sources, it would be geared toward helping young entrepreneurs and college students start their own businesses. "We really want to be known for our parties," Mack told a source, who asked to remain anonymous.</p>

				<p>Sources also said the business could entail a membership fee. As a member, students would have access to discounted items, such as travel or computers. According to sources, the business would have direct ties to NXIVM, something that NXIVM president Nancy Salzman flatly denied.</p>

				<p>In an e-mail to Metroland, Salzman wrote, "To say their business is affiliated with NXIUM [sic] is quite a stretch, but if you must it would also mean, Enron, Worldcom, Krispy Kreme and BNI, Black Entertainment TV and a number of different countries are NXIVM affiliated. . . . The business to which you refer is just another business started by one of our past or present 8,000 participants who took one of our courses in ethics, critical thinking and entrepreneurship. It is our intent to help people start businesses if they want to and be more joyful and prosperous in their lives, I guess we"re doing a good job."</p>

				<p>"What Raniere is obviously doing is gearing up for the college crowd," said Rick Ross, a leading cult deprogrammer and controversial critic of NXIVM. "The demographic that has been the most lucrative, the most fruitful for cults is 18 to 26."</p>

				<p>Young people with substantial discretionary funds who are alone for the first time in their lives, are an ideal target, he said. He pointed to the survey that Clyne and Mack have linked to from their official Web sites as an example of Raniere's attempts to gather data on this demographic. Nearly each page of the online survey features a picture of Clyne, Mack, or Kreuk. The survey is hosted at etho lutions.com, a domain registered to Karen Unterreiner, a longtime associate of Raniere's from his days with CBI.</p>

				<p>Raniere, according to Ross, is not allowed, by law, to be involved in a discount buyer's club, due to the collapse of CBI.</p>

				<p>The survey is specifically geared toward college students. It asks more than a dozen questions regarding their purchasing, studying, and recreational habits. After a round of these questions, the survey moves on to more unusual questions, such as, "Would you swallow a glass of your own vomit for $100?" The questions continue along in this vein, increasing the hypothetical monetary payout, and also the grossness factor: "What if it was dog vomit?"</p>

				<p>"My experience with Hollywood people and cults, they are really easy to grab," said Ross. "A lot easier than you would imagine. They are so vulnerable and emotionally needy. What kind of person wants to be an actor in the first place? They need to be loved, adored, and the center of attention. It's like shooting fish in a barrel."</p>

				<p>Clyne and Mack declined to comment for this article.</p>

				<p><i>Originally published by Metroland, March, 2008</i></p>
			</div>
		</article>
		<article class="section-break">
			<header class="article-header"><h2>Sue Happy</h2></header>
			<div class="content">
				<p><i>After years of battling NXIVM in court, a nationally known cult deprogrammer says the end is nigh</i></p>

				<p>This is the fifth lawsuit for Rick Ross. The New Jersey-based cult deprogrammer and founder of the Rick Ross Institute maintains a Web site that catalogues news coverage and allegations of cult activity, making him a target of what he calls harassment suits. While these legal battles can take up years of his life, he said, he's never lost a suit. And in some cases, he has been pleasantly surprised by the unintended outcomes.</p>

				<p>Take, for example, the suit leveled against him by the Maine-based Gentle Wind Project. That suit, Ross said, backfired on Gentle Wind by drawing the attention of Maine's attorney general. Until that point, the authorities hadn't paid much attention to the odd little group. "But they did start paying attention to them after they sued me," says Ross, "and then he shut them down--literally liquidated them."</p>

				<p>He was sued by Arizona's Church of the Immortal Consciousness, and by a woman in Florida who ran a ministry called the Pure Bride Ministries. He was sued by Landmark Education, who dismissed their own lawsuit, he said, "to get out from underneath it. As soon as they realized that they were going to lose they beat it--and quickly."</p>

				<p>"And now we have NXIVM," Ross said, "who has lost at every juncture. They have just lost and lost and lost and lost."</p>

				<p>Founded by Keith Raniere and Nancy Salzman, NXIVM is the Capital Region-based corporation behind Executive Success Programs, which claims to develop and impart "programs that provide the philosophical and practical foundation necessary to acquire and build the skills for success."</p>

				<p>As Ross is reporting on his site, Cult News.com, "a motion filed by NXIVM to reinstate causes of action previously dismissed in June of 2007 has been denied. This included an effort to reinstate claims of 'product disparagement' and 'tortious interference' in a long-standing lawsuit filed against the Ross Institute of New Jersey."</p>

				<p>"The motion by Raniere's lawyers," Ross told Metroland, "is a kind of Hail Mary pass. Their hope was to get these causes back in, because they realized if they didn't that the lawsuit had no hope whatsoever." These causes, he continued, "were the causes that had meat."</p>

				<p>What remains, now, are deliberations over trade secrets violations and copyright causes. "It's as though the judge has turned off the life support" for NXIVM's suit, Ross said. "Now it's just going to wiggle to death."</p>

				<p>He said that he believes in a year from now, the suit will finally reach its conclusion.</p>

				<p>"This suit never had any substance to it. From its inception, it has never been anything more than a harassment suit," Ross said. He laughed at the length of time that the suit has lasted. "Seven years. That's an all-time record with me. Most of my adversaries, the various cults that have sued me, they don't last five years. Raniere managed to keep it going for seven. You gotta give the guy some credit."</p>

				<p>While Ross maintained that it lacked substance, he added that the NXIVM suit "is probably the most significant lawsuit that I have been involved in from the standpoint of writing new law and establishing new precedent. The NXIVM v. Ross suit is now cited as an expansion of the First Amendment, as upheld by the United States Supreme Court, in regards to confidentiality agreements."</p>

				<p>To protect what Raniere believes are the trade secrets behind the technology that the organization uses to train its students, NXIVM requires that students sign confidentiality agreements. Ross received copies of NXIVM's training materials from a former student who had signed such an agreement, which was ignored when Ross posted the training materials, along with analysis, on his Web site.</p>

				<p>In 2003, NXIVM went to the courts to seek an emergency injunction against Ross' site, he said. The Albany court, where the claim was filed, ruled in Ross' favor. "So NXIVM appealed to the 2nd Circuit in Manhattan, and that court found that the confidentiality agreement was trumped by the public's right to know. NXIVM appealed that to the U.S. Supreme Court, saying that this is unprecedented, that confidentiality agreements in the past have been upheld in proceedings, and that this confidentiality should be upheld." The U.S. Supreme Court refused to hear the case. "And that gave the 2nd Circuit the leeway that they literally wrote new law. New law came out of this case."</p>

				<p>Ross has been a consistent source for this paper in its reporting on NXIVM.</p>

				<p>In a 2008 Metroland article, "The Stars Come Out in Troy," Ross was quoted discussing Raniere's previous business, Consumers Buyline Inc. Raniere later claimed that Ross was incorrect in his characterization of that company's legal issues and that Metroland had conspired with Ross to libel him and NXIVM.</p>

				<p>This past spring, it appeared that NXIVM had attempted to sue Metroland for $65 million. However, the company that owns Metroland, Lou Communications Inc., was never served in that suit. Instead, NXIVM attempted to serve the now-defunct former owner of the paper, Metroland Magazine Inc.</p>

				<p>The statue of limitations on NXIVM's potential claim against Metroland has expired.</p>

				<p>As for Ross, he estimated that his legal expenses to defend himself in NXIVM Corp. v. Ross would have topped $2 million had he not received extensive pro-bono representation. As it stands, he said, he has spent less than $1,000.</p>

				<p>Ross' countersuit against NXIVM is ongoing.</p>

				<p><i>Originally published by Metroland, December, 2009</i></p>
			</div>
		</article>
	</main>
<?php include('../footer.php'); ?>